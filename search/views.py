from django.shortcuts import render
from .forms import QueryForm
from search.services.softech.softech_main import SoftechMain
from search.services.myphone.myphone_main import MyphoneMain, ProductNotFound
from search.services.kivano.kivano_main import KivanoMain
from search.services.svetofor.svetofor_main import SvetovorMain


def search(request):
    if request.method == 'POST':
        form = QueryForm(request.POST)
        if form.is_valid():
            context = {
                'error': []
            }
            query = str(form.cleaned_data['query']).lower()
            """Вывод из myphone.kg"""
            try:
                myphone_result = MyphoneMain().myphonekg_main \
                    (user_query_name=query)
                context['myphone'] = myphone_result
            except ProductNotFound:
                context['error'].append(
                    {'myphone': "По запросу " + form.cleaned_data['query']
                                + " ничего не найдено"}
                )
            """Вывод из kivano.kg"""
            try:
                kivano_result = KivanoMain().kivano_main(user_query_name=query)
                context['kivano'] = kivano_result
            except:
                context['error'].append(
                    {'kivano': "По запросу " + form.cleaned_data['query']
                               + " ничего не найдено"})
            try:
                svetofor_result = SvetovorMain().svetofor_main \
                    (user_query_name=query)
                context['svetofor'] = svetofor_result
            except Exception:
                context['error'].append(
                    {'svetofor': "По запросу " + form.cleaned_data['query']
                                 + " ничего не найдено"})
            """Вывод из softech.kg"""
            try:
                softech_result = SoftechMain().softech_main \
                    (query=query)
                context['softech'] = softech_result
            except Exception:
                context['error'].append(
                    {'softech': "По запросу " + form.cleaned_data['query']
                                + " ничего не найдено"})
            return render(request, 'search/home.html', context)
        else:
            """Сделать ввывод ошибки в html"""
            error = form.errors
            return render(request, 'search/home.html', {'error': error})
    return render(request, 'search/home.html')

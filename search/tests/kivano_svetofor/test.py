from django.test import TestCase
from search.services.kivano.kivano_transform import KivanoTransform
from search.services.svetofor.transform import SvetoforTransform


class TestKivanoTransformer(TestCase):

    def test_form_base_url(self):
        transform = KivanoTransform()
        name_list = ['samsung']
        final_url = 'https://www.kivano.kg/mobilnye-telefony?search=samsung'
        base_url = transform.form_base_url(name_list=name_list)
        self.assertEqual(final_url, base_url)

    def test_get_product_urls_from_requested_page(self):
        kivano_transform = KivanoTransform()
        url = 'https://www.kivano.kg/mobilnye-telefony?search=apple%20iphone%2011'
        with open('search/tests/kivano_svetofor/kivano/requested_page.html', 'r') as f:
            html = f.read()
        product_links = kivano_transform.get_product_urls_from_requested_page(page=html, url=url)
        final_links = ['https://www.kivano.kg/mobilnye-telefony?search=apple%20iphone%2011&page=1',
                       'https://www.kivano.kg/mobilnye-telefony?search=apple%20iphone%2011&page=2',
                       'https://www.kivano.kg/mobilnye-telefony?search=apple%20iphone%2011&page=3',
                       'https://www.kivano.kg/mobilnye-telefony?search=apple%20iphone%2011&page=4']
        self.assertEqual(final_links, product_links)

    def test_get_data_from_product_pages(self):
        transform = KivanoTransform()
        with open('search/tests/kivano_svetofor/kivano/product_pages.html', 'r') as ff:
            pages = ff.read().split('baktybek')
        products_data = transform.get_data_from_product_pages(product_pages=pages)
        final_data = [
            {'name': 'Ноутбук Apple MacBook Pro 13 with Retina display Mid 2017 (MPXT2) серый', 'price': '105870',
             'url': 'https://www.kivano.kg/product/view/noutbuk-apple-macbook-pro-13-with-retina-display-mid-2017-mpxt2-seryy'},
            {'name': 'Ноутбук Apple MacBook Pro 13 with Retina display and Touch Bar Mid 2019 (MUHQ2) серебристый',
             'price': '105940',
             'url': 'https://www.kivano.kg/product/view/noutbuk-apple-macbook-pro-13-with-retina-display-and-touch-bar-mid-2019-muhq2-serebristyy'},
            {'name': 'Ноутбук Apple MacBook Pro 13 with Retina display and Touch Bar Mid 2019 (MUHP2) серый космос',
             'price': '121420',
             'url': 'https://www.kivano.kg/product/view/noutbuk-apple-macbook-pro-13-with-retina-display-and-touch-bar-mid-2019-muhp2-seryy-kosmos'},
            {'name': 'Ноутбук Apple MacBook Pro 15 with Retina display Mid 2019 (MV922) серебристый',
             'price': '162900',
             'url': 'https://www.kivano.kg/product/view/noutbuk-apple-macbook-pro-15-with-retina-display-mid-2019-mv922-serebristyy'},
            {'name': 'Ноутбук Apple MacBook Pro 16 with Retina display and Touch Bar Late 2019 (MVVJ2)',
             'price': '203660',
             'url': 'https://www.kivano.kg/product/view/noutbuk-apple-macbook-pro-16-with-retina-display-and-touch-bar-late-2019-mvvj2'},
            {'name': 'Ноутбук Apple MacBook Pro 16 with Retina display and Touch Bar Late 2019 (MVVK2) серый космос',
             'price': '219940',
             'url': 'https://www.kivano.kg/product/view/noutbuk-apple-macbook-pro-16-with-retina-display-and-touch-bar-late-2019-mvvk2-seryy-kosmos'}]
        self.assertEqual(final_data, products_data)


    def test_get_product_boxes(self):
        transform = KivanoTransform()
        with open('search/tests/kivano_svetofor/kivano/empty_page.html', 'r') as file:
            pages = file.read().split()
        response = transform._get_product_boxes(product_pages=pages)
        final = 'Неправильное название товара'
        self.assertEqual(response, final)


    def test_get_number_of_urls(self):
        transform = KivanoTransform()
        with open('search/tests/kivano_svetofor/kivano/empty_page.html', 'r') as fi:
            page = fi.read()
        urls_number = transform._get_number_of_urls(page=page)
        self.assertEqual(urls_number, 1)


class TestSvetoforTransformer(TestCase):

    def test_form_base_url(self):
        transform = SvetoforTransform()
        name_list = ['samsung']
        final_url = 'https://svetofor.info/sotovye-telefony-i-aksessuary/vse-smartfony?q=samsung&items_per_page=100'
        base_url = transform.form_base_url(name_list=name_list)
        self.assertEqual(final_url, base_url)

    def test_parse_page_raise_exception(self):
        transform = SvetoforTransform()
        with open('search/tests/kivano_svetofor/svetofor/empty_page.html', 'r') as file:
            pages = file.read()
        response = transform.parse_page(product_page=pages)
        final = 'Неправильное имя'
        self.assertEqual(response, final)

    def test_parse_page(self):
        transform = SvetoforTransform()
        with open('search/tests/kivano_svetofor/svetofor/product_page.html', 'r') as file:
            page = file.read()
            products_data = transform.parse_page(product_page=page)
            final_data = [{'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim серый космос',
                           'price': '112400',
                           'url': 'https://svetofor.info/apple-iphone-11-pro-max-512gb-single-sim-seryy-kosmos-z0000066495-72.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim серебристый',
                           'price': '112400',
                           'url': 'https://svetofor.info/apple-iphone-11-pro-max-512gb-single-sim-serebristyy-z0000066496-72.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim тёмно-зелёный',
                           'price': '112400',
                           'url': 'https://svetofor.info/apple-iphone-11-pro-max-512gb-single-sim-temno-zelenyy-z0000066497-72.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim золотой',
                           'price': '112400',
                           'url': 'https://svetofor.info/apple-iphone-11-pro-max-512gb-single-sim-zolotoy-z0000066494-72.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim золотой',
                           'price': '113200',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-zolotoj.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim серебристый',
                           'price': '113200',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-serebristyj.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim серый космос',
                           'price': '113200',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-seryj-kosmos.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim тёмно-зелёный',
                           'price': '113200',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-tjomno-zeljonyj.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim серый космос',
                           'price': '114400',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-seryy-kosmos.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim серебристый',
                           'price': '114400',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-serebristyy.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim тёмно-зелёный',
                           'price': '114400',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-temno-zelenyy.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim золотой',
                           'price': '114400',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-zolotoy.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim серый космос',
                           'price': '116000',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-seryj-kosmos-64.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim серебристый',
                           'price': '116000',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-serebristyj-64.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim золотой',
                           'price': '116000',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-zolotoj-64.html'},
                          {'name': 'Смартфон Apple iPhone 11 Pro Max 512 Gb (RAM 4 Gb) Single Sim тёмно-зелёный',
                           'price': '116000',
                           'url': 'https://svetofor.info/smartfon-apple-iphone-11-pro-max-512-gb-ram-4-gb-single-sim-tjomno-zeljonyj-64.html'}]
            self.assertEqual(products_data, final_data)

from django.test import TestCase
from search.forms import QueryForm


class FormsTestCase(TestCase):
    def test_valid(self):
        form = QueryForm({'query': 'samsung galaxy s8'})
        self.assertTrue(form.is_valid())

    def test_empy_field(self):
        form = QueryForm({'query': ''})
        self.assertFalse(form.is_valid())


import unittest
from search.services.softech.softech_transform import SoftechTransform

class transform_test(unittest.TestCase):
    def test_find_url(self):
        products_page = []
        final_url=\
            ['https://softech.kg/smartfon-apple-iphone-11-pro-max-256gb-1-sim',
            'https://softech.kg/smartfon-apple-iphone-11-pro-max-512gb']
        with open('search/tests/softech/html/transform_iphone1.html', 'r') as file1:
            html1 = file1.read()
        products_page.append(html1)
        with open('search/tests/softech/html/transform_iphone2.html', 'r') as file2:
            html2 = file2.read()
        products_page.append(html2)
        transform=SoftechTransform()
        products_url=transform.find_url_names(url=products_page)
        self.assertEqual(final_url,products_url)

    def test_find_names(self):
        final_names = [
            ['Смартфон Apple iPhone 11 Pro Max 256GB'],
            ['Смартфон Apple iPhone 11 Pro Max 512GB']]
        products_page=[]
        with open('search/tests/softech/html/transform_iphone1.html', 'r') as file1:
            html1=file1.read()
        products_page.append(html1)
        with open('search/tests/softech/html/transform_iphone2.html', 'r') as file2:
            html2 = file2.read()
        products_page.append(html2)
        transform = SoftechTransform()
        products_name = transform.find_name(url=products_page)
        self.assertEqual(final_names, products_name)
    def test_find_colors_and_memory(self):
        final_colors_and_memory = [['256GB', 'Белый', 'Золотистый', 'Темно - серый', 'Зеленый'],
                                   ['512GB', 'Белый', 'Золотистый', 'Темно - серый', 'Зеленый']]
        products_page=[]
        with open('search/tests/softech/html/transform_iphone1.html', 'r') as file1:
            html1 = file1.read()
        products_page.append(html1)
        with open('search/tests/softech/html/transform_iphone2.html', 'r') as file2:
            html2 = file2.read()
        products_page.append(html2)
        transform = SoftechTransform()
        memory_and_colors = transform.find_color_and_memory(url=products_page)
        self.assertEqual(final_colors_and_memory, memory_and_colors)

    def test_find_color_and_memory(self):
        final_colors_and_memory = [['(6+64)', 'Белый', 'Черный', 'Зеленый']]
        products_page = []
        with open('search/tests/softech/html/transform_xiaomi.html', 'r') as file:
            html = file.read()
        products_page.append(html)
        transform = SoftechTransform()
        memory_and_colors = transform.find_color_and_memory(url=products_page)
        self.assertEqual(final_colors_and_memory, memory_and_colors)

    def test_find_prices(self):
        final_price = [['16850']]
        transform = SoftechTransform()
        products_page=[]
        with open('search/tests/softech/html/transform_xiaomi.html', 'r') as file:
            html = file.read()
        products_page.append(html)
        products_price = transform.find_price(url=products_page)
        self.assertEqual(final_price, products_price)

    def test_response_to_user(self):
        response = [{'name': 'Смартфон Xiaomi Redmi Note 8 Pro (6+64) EU', 'price': 16850,
                     'params': '(6+64), Белый, Черный, Зеленый',
                     'url':'https://softech.kg/smartfon-xiaomi-redmi-note-8-pro-664-eu'}]
        transform = SoftechTransform()
        products_page = []
        with open('search/tests/softech/html/transform_xiaomi.html', 'r') as file:
            html = file.read()
        products_page.append(html)
        response_to_user = transform.response_to_user(url=products_page)
        self.assertEqual(response, response_to_user)

    def test_response_to_user2(self):
        response = [
        {'name': 'Смартфон Apple iPhone 11 Pro Max 256GB', 'price': 96950,
         'params': '256GB, Белый, Золотистый, Темно - серый, Зеленый',
         'url': 'https://softech.kg/smartfon-apple-iphone-11-pro-max-256gb-1-sim'},
        {'name': 'Смартфон Apple iPhone 11 Pro Max 512GB', 'price': 101500,
         'params': '512GB, Белый, Золотистый, Темно - серый, Зеленый',
         'url': 'https://softech.kg/smartfon-apple-iphone-11-pro-max-512gb'}]
        products_page = []
        with open('search/tests/softech/html/transform_iphone1.html', 'r') as file1:
            html1 = file1.read()
        products_page.append(html1)
        with open('search/tests/softech/html/transform_iphone2.html', 'r') as file2:
            html2 = file2.read()
        products_page.append(html2)
        transform = SoftechTransform()
        response_to_user = transform.response_to_user(url=products_page)
        self.assertEqual(response, response_to_user)



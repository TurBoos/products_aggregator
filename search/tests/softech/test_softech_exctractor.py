import unittest
from unittest import TestCase
from search.services.softech.softech_exctract import SoftechExtractor

TestCase.maxDiff = None


class exctractor_test(unittest.TestCase):
    def test_get_url(self):
        final ='https://softech.kg/index.php?route=product/search&search=Смартфон%20Apple%20iPhone%2011%20Pro%20Max%20256GB&category_id=59&sub_category=true&limit=25'
        name = 'Смартфон Apple iPhone 11 Pro Max 256GB'
        exctractor = SoftechExtractor()
        response = exctractor.get_url(product_name=name)
        self.assertEqual(final, response)
    def test_product_availability(self):
        final='Есть в наличии'
        with open('search/tests/softech/html/exctractor_iphone1.html') as file:
            html=file.read()
        exctractor=SoftechExtractor()
        response=exctractor.product_availability(product_html=html)
        self.assertEqual(final,response)



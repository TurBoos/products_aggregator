from django.test import TestCase
from django.utils import timezone
from search.models import Query
import datetime


class QueryTestCase(TestCase):
    def setUp(self):
        query = Query.objects.create(query='samsung galaxy s8',
                                     time=timezone.now())
        query.products_set.create(name='samsung galaxy s8 pro',
                                  price=80000,
                                  parametrs='Желтый,250gb',
                                  url='www/samsung.com')

    def test_query_response(self):
        query = Query.objects.get(query='samsung galaxy s8')
        self.assertEqual('samsung galaxy s8', str(query))

    def test_products_response(self):
        query = Query.objects.get(query='samsung galaxy s8')
        products = query.products_set.get(name='samsung galaxy s8 pro')
        self.assertEqual(str(products.name), 'samsung galaxy s8 pro')
        self.assertEqual(products.price, 80000)
        self.assertEqual(products.parametrs, 'Желтый,250gb')
        self.assertEqual(products.url, 'www/samsung.com')

    def test_was_published_recently_with_old_question(self):
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Query(time=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Query(time=time)
        self.assertIs(recent_question.was_published_recently(), True)

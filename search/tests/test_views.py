from django.test import TestCase, Client
from unittest.mock import patch
from django.utils import timezone
from search.models import Query, Products
from search.services.myphone.exceptions import ProductNotFound


class ViewsTestCase(TestCase):
    def setUp(self):
        self.query = Query.objects.create(query='Samsung', time=timezone.now())
        self.query_number_two = Query.objects.create(query='Samsung galaxy',
                                                     time=timezone.now())

        self.query.products_set.create(
            name='Samsung Galaxy S10+',
            parametrs='Чёрный Зеленый Белый',
            price=55590,
            url='https://myphone.kg/ru/catalog/cell/i1507/galaxy_s10',
            web_site='myphone')
        self.query.products_set.create(
            name='Samsung Galaxy S10',
            parametrs='Чёрный Зеленый Белый',
            price=48390,
            url='https://myphone.kg/ru/catalog/cell/i1506/galaxy_s10',
            web_site='myphone')
        self.query.products_set.create(
            name='Samsung Galaxy S10 Lite 128Gb',
            parametrs='Чёрный Белый Синий 128Gb',
            price=39990,
            url='https://myphone.kg/ru/catalog/cell/i1948/galaxy_s10_lite',
            web_site='myphone')

        self.query.products_set.create(
            name='Сотовый телефон Samsung Galaxy S10 Lite 6/128GB (SM-G770F/DS) белый',
            price=28210,
            url='https://www.kivano.kg/product/view/sotovyy-telefon-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-belyy',
            web_site='kivano')
        self.query.products_set.create(
            name='Сотовый телефон Samsung Galaxy S10 Lite 6/128GB (SM-G770F/DS) черный',
            price=28210,
            url='https://www.kivano.kg/product/view/sotovyy-telefon-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-belyy',
            web_site='kivano')
        self.query.products_set.create(
            name='Сотовый телефон Samsung Galaxy S10 Lite 6/128GB (SM-G770F/DS) красный',
            price=28210,
            url='https://www.kivano.kg/product/view/sotovyy-telefon-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-belyy',
            web_site='kivano')

        self.query.products_set.create(
            name='Смартфон Samsung Galaxy S10e G970F 128 Gb (RAM 6 Gb) Dual Sim РСТ green',
            price='34741',
            url='https://svetofor.info/samsung-galaxy-s10e-g970f-128gb-ram-6gb-dual-sim-rst-zelenyy-akvamarin-z0000060802-72.html',
            web_site='svetofor')
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy S10e SM-G970F 128 Gb (RAM 6 Gb) Dual Sim PCT prism white',
            price='34741',
            url='https://svetofor.info/samsung-galaxy-s10e-g970f-128gb-ram-6gb-dual-sim-rst-belyy-perlamutr-z0000062973-72.html',
            web_site='svetofor')
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy S10e G970F 128 Gb (RAM 6 Gb) Dual Sim Prism White',
            price='34741',
            url='https://myphone.kg/ru/catalog/cell/i1948/galaxy_s10_lite',
            web_site='svetofor')

        self.query.products_set.create(
            name='Смартфон Samsung Galaxy A11 (2+32) EU',
            parametrs='(2+32), Черный',
            price=9950,
            url='https://softech.kg/smartfon-samsung-galaxy-a11-232-eu',
            web_site='softech')
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy A21s (3+32) EU',
            parametrs='(3+32), Черный, Синий',
            price=12750,
            url='https://softech.kg/smartfon-samsung-galaxy-a21s-332-eu',
            web_site='softech')
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy A31 (4+128) EU',
            parametrs='(4+128), Черный, Синий',
            price=16950,
            url='https://softech.kg/smartfon-samsung-galaxy-a31-4128-eu',
            web_site='softech')

    def test_home(self):
        client = Client()
        response = client.get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'search/home.html')

    @patch('search.views.MyphoneMain.myphonekg_main')
    @patch('search.views.KivanoMain.kivano_main')
    @patch('search.views.SvetovorMain.svetofor_main')
    @patch('search.views.SoftechMain.softech_main')
    def test_products_response \
                    (self, softech_mock, svetofor_mock, kivano_mock,
                     myphone_mock):
        """Тест вывода при наличии всех продуктов"""
        myphone_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='myphone')
        kivano_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='kivano')
        svetofor_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='svetofor')
        softech_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='softech')
        client = Client()
        response = client.post('', {'query': 'samsung'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['myphone'], myphone_mock.return_value)
        self.assertEqual(response.context['kivano'], kivano_mock.return_value)
        self.assertEqual(response.context['svetofor'],
                         svetofor_mock.return_value)
        self.assertEqual(response.context['softech'], softech_mock.return_value)

    @patch('search.views.MyphoneMain.myphonekg_main')
    @patch('search.views.KivanoMain.kivano_main')
    @patch('search.views.SvetovorMain.svetofor_main')
    @patch('search.views.SoftechMain.softech_main')
    def test_products_response2 \
                    (self, softech_mock, svetofor_mock, kivano_mock,
                     myphone_mock):
        """Тест,вывода продуктов , в котором продукты в myphone не были найдены"""
        myphone_mock.side_effect = ProductNotFound('Товар не найден')
        kivano_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='kivano')
        svetofor_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='svetofor')
        softech_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='softech')
        client = Client()
        response = client.post('', {'query': 'Samsung'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'][0],
                         {'myphone': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['kivano'], kivano_mock.return_value)
        self.assertEqual(response.context['svetofor'],
                         svetofor_mock.return_value)
        self.assertEqual(response.context['softech'], softech_mock.return_value)

    @patch('search.views.MyphoneMain.myphonekg_main')
    @patch('search.views.KivanoMain.kivano_main')
    @patch('search.views.SvetovorMain.svetofor_main')
    @patch('search.views.SoftechMain.softech_main')
    def test_products_response3 \
                    (self, softech_mock, svetofor_mock, kivano_mock,
                     myphone_mock):
        """Тест,вывода продуктов , в котором продукты в kivano не были найдены"""
        myphone_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='myphone')
        kivano_mock.side_effect = Exception()
        svetofor_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='svetofor')
        softech_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='softech')
        client = Client()
        response = client.post('', {'query': 'Samsung'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['myphone'], myphone_mock.return_value)
        self.assertEqual(response.context['error'][0],
                         {'kivano': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['svetofor'],
                         svetofor_mock.return_value)
        self.assertEqual(response.context['softech'], softech_mock.return_value)

    @patch('search.views.MyphoneMain.myphonekg_main')
    @patch('search.views.KivanoMain.kivano_main')
    @patch('search.views.SvetovorMain.svetofor_main')
    @patch('search.views.SoftechMain.softech_main')
    def test_products_response4 \
                    (self, softech_mock, svetofor_mock, kivano_mock,
                     myphone_mock):
        """Тест,вывода продуктов , в котором продукты в svetofor не были найдены"""
        myphone_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='myphone')
        kivano_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='kivano')
        svetofor_mock.side_effect = Exception('')
        softech_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='softech')
        client = Client()
        response = client.post('', {'query': 'Samsung'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['myphone'], myphone_mock.return_value)
        self.assertEqual(response.context['kivano'], kivano_mock.return_value)
        self.assertEqual(response.context['error'][0],
                         {'svetofor': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['softech'], softech_mock.return_value)

    @patch('search.views.MyphoneMain.myphonekg_main')
    @patch('search.views.KivanoMain.kivano_main')
    @patch('search.views.SvetovorMain.svetofor_main')
    @patch('search.views.SoftechMain.softech_main')
    def test_products_response5 \
                    (self, softech_mock, svetofor_mock, kivano_mock,
                     myphone_mock):
        """Тест,вывода продуктов , в котором продукты в softech не были найдены"""
        myphone_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='myphone')
        kivano_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='kivano')
        svetofor_mock.return_value = Products.objects.filter \
            (query_id=self.query, web_site='svetofor')
        softech_mock.side_effect = Exception('')
        client = Client()
        response = client.post('', {'query': 'Samsung'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['myphone'], myphone_mock.return_value)
        self.assertEqual(response.context['kivano'], kivano_mock.return_value)
        self.assertEqual(response.context['svetofor'],
                         svetofor_mock.return_value)
        self.assertEqual(response.context['error'][0],
                         {'softech': 'По запросу Samsung ничего не найдено'})

    @patch('search.views.MyphoneMain.myphonekg_main')
    @patch('search.views.KivanoMain.kivano_main')
    @patch('search.views.SvetovorMain.svetofor_main')
    @patch('search.views.SoftechMain.softech_main')
    def test_products_response6 \
                    (self, softech_mock, svetofor_mock, kivano_mock,
                     myphone_mock):
        """Тест,вывода продуктов ,
         в котором продукты в myphone,kivano не были найдены"""
        myphone_mock.side_effect = ProductNotFound('Товар не найден')
        kivano_mock.side_effect = Exception('')
        svetofor_mock.return_value = Products.objects.filter(
            query_id=self.query, web_site='svetofor')
        softech_mock.return_value = Products.objects.filter(query_id=self.query,
                                                            web_site='softech')
        client = Client()
        response = client.post('', {'query': 'Samsung'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'][0],
                         {'myphone': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['error'][1],
                         {'kivano': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['svetofor'],
                         svetofor_mock.return_value)
        self.assertEqual(response.context['softech'], softech_mock.return_value)

    @patch('search.views.MyphoneMain.myphonekg_main')
    @patch('search.views.KivanoMain.kivano_main')
    @patch('search.views.SvetovorMain.svetofor_main')
    @patch('search.views.SoftechMain.softech_main')
    def test_products_response7 \
                    (self, softech_mock, svetofor_mock, kivano_mock,
                     myphone_mock):
        """Тест,вывода продуктов , в котором продукты в myphone,kivano,svetofor не были найдены"""
        myphone_mock.side_effect = ProductNotFound('Товар не найден')
        kivano_mock.side_effect = Exception('')
        svetofor_mock.side_effect = Exception('')
        softech_mock.return_value = Products.objects.filter(query_id=self.query,
                                                            web_site='softech')
        client = Client()
        response = client.post('', {'query': 'Samsung'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'][0],
                         {'myphone': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['error'][1],
                         {'kivano': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['error'][2],
                         {'svetofor': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['softech'], softech_mock.return_value)

    @patch('search.views.MyphoneMain.myphonekg_main')
    @patch('search.views.KivanoMain.kivano_main')
    @patch('search.views.SvetovorMain.svetofor_main')
    @patch('search.views.SoftechMain.softech_main')
    def test_products_response8(self, softech_mock,
                                svetofor_mock, kivano_mock, myphone_mock):
        """Тест,вывода продуктов ,
         в котором продукты в myphone,kivano,svetofor,softech не были найдены"""
        myphone_mock.side_effect = ProductNotFound('Товар не найден')
        kivano_mock.side_effect = Exception('')
        svetofor_mock.side_effect = Exception('')
        softech_mock.side_effect = Exception('')
        client = Client()
        response = client.post('', {'query': 'Samsung'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'][0],
                         {'myphone': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['error'][1],
                         {'kivano': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['error'][2],
                         {'svetofor': 'По запросу Samsung ничего не найдено'})
        self.assertEqual(response.context['error'][3],
                         {'softech': 'По запросу Samsung ничего не найдено'})

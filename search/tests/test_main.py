from search.models import Query, Products
from django.utils import timezone
from unittest.mock import patch
from django.test import TestCase
from search.services.myphone.exceptions import ProductNotFound
from search.services.myphone.myphone_main import MyphoneMain
from search.services.kivano.kivano_main import KivanoMain
from search.services.svetofor.svetofor_main import SvetovorMain
from search.services.softech.softech_main import SoftechMain
import datetime


class MyphoneMainTestCase(TestCase):
    def setUp(self):
        self.query = Query.objects.create(query='Samsung Galaxy S10',
                                          time=timezone.now())
        self.query_number_two = Query.objects.create(query='Samsung',
                                                     time=timezone.now())
        self.query.products_set.create(
            name='Samsung Galaxy S10+',
            parametrs='Чёрный Зеленый Белый',
            price=55590,
            url='https://myphone.kg/ru/catalog/cell/i1507/galaxy_s10',
            web_site='myphone')
        self.query.products_set.create(
            name='Samsung Galaxy S10',
            parametrs='Чёрный Зеленый Белый',
            price=48390,
            url='https://myphone.kg/ru/catalog/cell/i1506/galaxy_s10',
            web_site='myphone')
        self.query.products_set.create(
            name='Samsung Galaxy S10 Lite 128Gb',
            parametrs='Чёрный Белый Синий 128Gb',
            price=39990,
            url='https://myphone.kg/ru/catalog/cell/i1948/galaxy_s10_lite',
            web_site='myphone')

    @patch(
        'search.services.myphone.myphone_main.MyphoneMain.get_data_from_site')
    def test_error_with_unsaved_query(self, mock):
        """Когда в базе данных не сохранен запрос"""
        mock.return_value = 'Товар не найден'
        self.assertRaises(ProductNotFound, MyphoneMain().myphonekg_main,
                          'Продукт,которого нет')

    @patch(
        'search.services.myphone.myphone_main.MyphoneMain.get_data_from_site')
    def test_error_with_saved_query(self, mock):
        """Когда в базе данных сохранен запрос"""
        mock.return_value = 'Товар не найден'
        self.assertRaises(ProductNotFound, MyphoneMain().myphonekg_main,
                          'Samsung')

    @patch(
        'search.services.myphone.myphone_main.MyphoneMain.get_data_from_site')
    def test_error_with_outdated_request(self, mock):
        """Когда в базе данных сохранен запрос,но он не актуален ,
                т.к он был сохранен более одного дня назад"""
        mock.return_value = 'Товар не найден'
        times = timezone.now() - datetime.timedelta(days=1, seconds=1)
        self.query.time = times
        self.query.save()
        self.assertRaises(ProductNotFound, MyphoneMain().myphonekg_main,
                          'Samsung Galaxy S10')

    @patch(
        'search.services.myphone.myphone_main.MyphoneMain.get_data_from_site')
    def test_request_without_products(self, mock):
        """Cлучай,в котором запрос существует, но продукты еще не были созданы к нему"""
        mock.return_value = [
            {'name': 'Samsung Lite', 'params': 'Чёрный Зеленый', 'price': 55590,
             'url': 'https://myphone.kg/ru/catalog/cell/i1507/galaxy_s10'},
            {'name': 'Samsung J6', 'params': 'Чёрный Зеленый Белый',
             'price': 48390,
             'url': 'https://myphone.kg/ru/catalog/cell/i1506/galaxy_s10'},
            {'name': 'Samsung J8', 'params': 'Чёрный Белый Синий 128Gb',
             'price': 39990,
             'url': 'https://myphone.kg/ru/catalog/cell/i1948/galaxy_s10_lite'}
        ]
        products = MyphoneMain().myphonekg_main('Samsung')
        self.assertEqual(3, products.count())

    @patch(
        'search.services.myphone.myphone_main.MyphoneMain.get_data_from_site')
    def test_of_creatin_a_request_and_products_to_it(self, mock):
        """Случай,при котором создаетя новый запрос и продукты к нему"""
        mock.return_value = [
            {'name': 'Samsung Galaxy S20 Ultra 128Gb',
             'params': 'Чёрный Тёмно-серый 128Gb', 'price': 95990,
             'url': 'https://myphone.kg/ru/catalog/cell/i1953/galaxy_s20_ultra'},
            {'name': 'Samsung Galaxy S20+ 128Gb',
             'params': 'Чёрный Тёмно-серый Голубой 128Gb', 'price': 71790,
             'url': 'https://myphone.kg/ru/catalog/cell/i1952/galaxy_s20'},
            {'name': 'Samsung Galaxy S20 128Gb',
             'params': 'Красный Тёмно-серый Голубой 128Gb', 'price': 64390,
             'url': 'https://myphone.kg/ru/catalog/cell/i1951/galaxy_s20'}
        ]
        products = MyphoneMain().myphonekg_main('Samsung Galaxy S20')
        self.assertEqual(3, products.count())

    def test_with_pre_created_product_and_request(self):
        """Случай , при котором продукты и запрос уже были созданы"""
        products = MyphoneMain().myphonekg_main('Samsung Galaxy S10')
        self.assertEqual(3, products.count())

    @patch(
        'search.services.myphone.myphone_main.MyphoneMain.get_data_from_site')
    def test_for_outdated_request(self, mock):
        """Случай , при котором продукты и запрос уже были созданы,
            но запрос не актуален ,т.к он был сохранен более одного дня назад"""
        mock.return_value = [
            {'name': 'Samsung Galaxy S10+', 'params': 'Чёрный Зеленый Белый',
             'price': 55590,
             'url': 'https://myphone.kg/ru/catalog/cell/i1507/galaxy_s10'},
            {'name': 'Samsung Galaxy S10', 'params': 'Чёрный Зеленый Белый',
             'price': 48390,
             'url': 'https://myphone.kg/ru/catalog/cell/i1506/galaxy_s10'},
            {'name': 'Samsung Galaxy S10 Lite 128Gb',
             'params': 'Чёрный Белый Синий 128Gb',
             'price': 39990,
             'url': 'https://myphone.kg/ru/catalog/cell/i1948/galaxy_s10_lite'}]

        times = timezone.now() - datetime.timedelta(days=1, seconds=1)
        self.query.time = times
        self.query.save()
        products = MyphoneMain().myphonekg_main('Samsung Galaxy S10')
        self.assertEqual(3, products.count())


class KivanoMainTestCase(TestCase):
    def setUp(self):
        self.query = Query.objects.create(
            query='Сотовый телефон Samsung Galaxy S10 Lite 6/128GB (SM-G770F/DS)',
            time=timezone.now())
        self.query_number_two = Query.objects.create(query='Samsung',
                                                     time=timezone.now())
        self.query.products_set.create(
            name='Сотовый телефон Samsung Galaxy'
                 ' S10 Lite 6/128GB (SM-G770F/DS) белый',
            price=28210,
            url='https://www.kivano.kg/product/view/sotovyy-telefon-'
                'samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-belyy',
            web_site='kivano')
        self.query.products_set.create(
            name='Сотовый телефон Samsung Galaxy'
                 ' S10 Lite 6/128GB (SM-G770F/DS) черный',
            price=28210,
            url='https://www.kivano.kg/product/view/sotovyy-telefon-'
                'samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-belyy',
            web_site='kivano')
        self.query.products_set.create(
            name='Сотовый телефон Samsung Galaxy'
                 ' S10 Lite 6/128GB (SM-G770F/DS) красный',
            price=28210,
            url='https://www.kivano.kg/product/view/sotovyy-telefon'
                '-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-belyy',
            web_site='kivano')

    @patch(
        'search.services.kivano.kivano_main.KivanoMain.search_products_for_site')
    def test_error_with_unsaved_query(self, mock):
        """Когда в базе данных не сохранен запрос"""
        mock.return_value = []
        self.assertRaises(Exception, KivanoMain().kivano_main,
                          'Товар,которого нет')

    @patch(
        'search.services.kivano.kivano_main.KivanoMain.search_products_for_site')
    def test_error_with_saved_query(self, mock):
        """Когда в базе данных сохранен запрос"""
        mock.return_value = []
        self.assertRaises(Exception, KivanoMain().kivano_main, 'Samsung')

    @patch(
        'search.services.kivano.kivano_main.KivanoMain.search_products_for_site')
    def test_error_with_outdated_request(self, mock):
        """Когда в базе данных сохранен запрос,но он не актуален ,
                т.к он был сохранен более одного дня назад"""
        mock.return_value = []
        times = timezone.now() - datetime.timedelta(days=1, seconds=1)
        self.query.time = times
        self.query.save()
        self.assertRaises(Exception, KivanoMain().kivano_main,
                          'Сотовый телефон Samsung Galaxy'
                          ' S10 Lite 6/128GB (SM-G770F/DS)')

    @patch(
        'search.services.kivano.kivano_main.KivanoMain.search_products_for_site')
    def test_request_without_products(self, mock):
        """Cлучай,в котором запрос существует, но продукты еще не были созданы к нему"""
        mock.return_value = [
            {'name': 'Сотовый телефон Samsung Galaxy белый', 'price': '28210',
             'url': 'https://www.kivano.kg/product/view/sotovyy-telefon'
                    '-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-belyy'},
            {'name': 'Сотовый телефон Samsung синий', 'price': '31690',
             'url': 'https://www.kivano.kg/product/view/sotovyy-telefon'
                    '-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-siniy'},
            {'name': 'Сотовый телефон Samsung черный', 'price': '31690',
             'url': 'https://www.kivano.kg/product/view/sotovyy-telefon'
                    '-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-chernyy'}]
        products = KivanoMain().kivano_main('Samsung')
        self.assertEqual(3, products.count())

    @patch(
        'search.services.kivano.kivano_main.KivanoMain.search_products_for_site')
    def test_of_creatin_a_request_and_products_to_it(self, mock):
        """Случай,при котором создаетя новый запрос и продукты к нему"""
        mock.return_value = [
            {'name': 'Samsung Galaxy S20 Ultra 128Gb',
             'params': 'Чёрный Тёмно-серый 128Gb', 'price': 95990,
             'url': 'https://myphone.kg/ru/catalog/cell/i1953/galaxy_s20_ultra'},
            {'name': 'Samsung Galaxy S20+ 128Gb',
             'params': 'Чёрный Тёмно-серый Голубой 128Gb', 'price': 71790,
             'url': 'https://myphone.kg/ru/catalog/cell/i1952/galaxy_s20'},
            {'name': 'Samsung Galaxy S20 128Gb',
             'params': 'Красный Тёмно-серый Голубой 128Gb', 'price': 64390,
             'url': 'https://myphone.kg/ru/catalog/cell/i1951/galaxy_s20'}
        ]
        products = KivanoMain().kivano_main('Samsung Galaxy S20')
        self.assertEqual(3, products.count())

    def test_with_pre_created_product_and_request(self):
        """Случай , при котором продукты и запрос уже были созданы"""
        products = KivanoMain().kivano_main(
            'Сотовый телефон Samsung Galaxy S10 Lite 6/128GB (SM-G770F/DS)')
        self.assertEqual(3, products.count())

    @patch(
        'search.services.kivano.kivano_main.KivanoMain.search_products_for_site')
    def test_for_outdated_request(self, mock):
        """Случай , при котором продукты и запрос уже были созданы,
            но запрос не актуален ,т.к он был сохранен более одного дня назад"""
        mock.return_value = [
            {
                'name': 'Сотовый телефон Samsung Galaxy'
                        ' S10 Lite 6/128GB (SM-G770F/DS) белый',
                'price': '28210',
                'url': 'https://www.kivano.kg/product/view/sotovyy-telefon'
                       '-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-belyy'},
            {
                'name': 'Сотовый телефон Samsung Galaxy'
                        ' S10 Lite 6/128GB (SM-G770F/DS) синий',
                'price': '31690',
                'url': 'https://www.kivano.kg/product/view/sotovyy-telefon'
                       '-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-siniy'},
            {
                'name': 'Сотовый телефон Samsung Galaxy'
                        ' S10 Lite 6/128GB (SM-G770F/DS) черный',
                'price': '31690',
                'url': 'https://www.kivano.kg/product/view/sotovyy-telefon'
                       '-samsung-galaxy-s10-lite-6-128gb-sm-g770f-ds-chernyy'}
        ]
        times = timezone.now() - datetime.timedelta(days=1, seconds=1)
        self.query.time = times
        self.query.save()
        products = KivanoMain().kivano_main(
            'Сотовый телефон Samsung Galaxy S10 Lite 6/128GB (SM-G770F/DS)')
        products_names = ""
        for product in products:
            products_names += product.name + ' '
        self.assertEqual(
            'Сотовый телефон Samsung Galaxy S10 Lite 6/128GB (SM-G770F/DS) белый',
            products[0].name)
        self.assertEqual(3, products.count())


class SvetoforMainTestCase(TestCase):

    def setUp(self):
        self.query = Query.objects.create(query='Samsung Galaxy S10',
                                          time=timezone.now())
        self.query_number_two = Query.objects.create(query='Samsung',
                                                     time=timezone.now())
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy'
                 ' S10e G970F 128 Gb (RAM 6 Gb) Dual Sim РСТ green',
            price='34741',
            url='https://svetofor.info/samsung-galaxy'
                '-s10e-g970f-128gb-ram-6gb-dual-sim-rst'
                '-zelenyy-akvamarin-z0000060802-72.html',
            web_site='svetofor')
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy'
                 ' S10e SM-G970F 128 Gb (RAM 6 Gb) Dual Sim PCT prism white',
            price='34741',
            url='https://svetofor.info/samsung-galaxy'
                '-s10e-g970f-128gb-ram-6gb-dual-sim-rst'
                '-belyy-perlamutr-z0000062973-72.html',
            web_site='svetofor')
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy'
                 ' S10e G970F 128 Gb (RAM 6 Gb) Dual Sim Prism White',
            price='34741',
            url='https://myphone.kg/ru/catalog/cell/i1948/galaxy_s10_lite',
            web_site='svetofor')

    @patch(
        'search.services.svetofor.svetofor_main.SvetovorMain.search_products_for_site')
    def test_error_with_unsaved_query(self, mock):
        """Когда в базе данных не сохранен запрос"""
        mock.return_value = 'Неправильное имя'
        self.assertRaises(Exception, SvetovorMain().svetofor_main,
                          'Продукт,которого нет')

    @patch(
        'search.services.svetofor.svetofor_main.SvetovorMain.search_products_for_site')
    def test_error_with_saved_query(self, mock):
        """Когда в базе данных сохранен запрос"""
        mock.return_value = 'Неправильное имя'
        self.assertRaises(Exception, SvetovorMain().svetofor_main, 'Samsung')

    @patch(
        'search.services.svetofor.svetofor_main.SvetovorMain.search_products_for_site')
    def test_error_with_outdated_request(self, mock):
        """Когда в базе данных сохранен запрос,но он не актуален ,
                т.к он был сохранен более одного дня назад"""
        mock.return_value = 'Неправильное имя'
        times = timezone.now() - datetime.timedelta(days=1, seconds=1)
        self.query.time = times
        self.query.save()
        self.assertRaises(Exception, SvetovorMain().svetofor_main,
                          'Samsung Galaxy S10')

    @patch(
        'search.services.svetofor.svetofor_main.SvetovorMain.search_products_for_site')
    def test_request_without_products(self, mock):
        """Cлучай,в котором запрос существует, но продукты еще не были созданы к нему"""
        mock.return_value = [
            {
                'name': 'Смартфон Samsung Galaxy'
                        ' A01 (2020) 16 Gb (RAM 2 Gb) Dual Sim black',
                'price': '7566',
                'url': 'https://svetofor.info/smartfon'
                       '-samsung-galaxya012020-16-gb-ram-2-gb-dual-sim-black.html'},
            {
                'name': 'Смартфон Samsung Galaxy'
                        ' A01 (2020) 16 Gb (RAM 2 Gb) Dual Sim red',
                'price': '7566',
                'url': 'https://svetofor.info/smartfon'
                       '-samsung-galaxya012020-16-gb-ram-2-gb-dual-sim-red.html'},
            {
                'name': 'Смартфон Samsung Galaxy'
                        ' A01 (2020) 16 Gb (RAM 2 Gb) Dual Sim blue',
                'price': '7566',
                'url': 'https://svetofor.info/smartfon'
                       '-samsung-galaxya012020-16-gb-ram-2-gb-dual-sim-blue.html'},
        ]
        products = SvetovorMain().svetofor_main('Samsung')
        self.assertEqual(3, products.count())

    def test_with_pre_created_product_and_request(self):
        """Случай , при котором продукты и запрос уже были созданы"""
        products = SvetovorMain().svetofor_main('Samsung Galaxy S10')
        self.assertEqual(3, products.count())

    @patch(
        'search.services.svetofor.svetofor_main.SvetovorMain.search_products_for_site')
    def test_for_outdated_request(self, mock):
        """Случай , при котором продукты и запрос уже были созданы,
            но запрос не актуален ,т.к он был сохранен более одного дня назад"""
        mock.return_value = [
            {
                'name': 'Смартфон Samsung Galaxy'
                        ' S10e G970F 128 Gb (RAM 6 Gb) Dual Sim РСТ green',
                'price': '34741',
                'url': 'https://svetofor.info/samsung-galaxy-s10e-g970f-128gb'
                       '-ram-6gb-dual-sim-rst-zelenyy-akvamarin-z0000060802-72.html'},
            {
                'name': 'Смартфон Samsung Galaxy'
                        ' S10e SM-G970F 128 Gb (RAM 6 Gb) Dual Sim PCT prism white',
                'price': '34741',
                'url': 'https://svetofor.info/samsung-galaxy-s10e-g970f-128gb'
                       '-ram-6gb-dual-sim-rst-belyy-perlamutr-z0000062973-72.html'},
            {
                'name': 'Смартфон Samsung Galaxy'
                        ' S10e G970F 128 Gb (RAM 6 Gb) Dual Sim Prism White',
                'price': '34741',
                'url': 'https://svetofor.info/samsung-galaxy-s10e-g970f-128gb'
                       '-ram-6gb-dual-sim-belyy-perlamutr-z0000057490-72.html'}
        ]
        times = timezone.now() - datetime.timedelta(days=1, seconds=1)
        self.query.time = times
        self.query.save()
        products = SvetovorMain().svetofor_main('Samsung Galaxy S10')
        self.assertEqual(3, products.count())


class SvetoforMainTestCase(TestCase):
    def setUp(self):
        self.query = Query.objects.create(query='Samsung Galaxy',
                                          time=timezone.now())
        self.query_number_two = Query.objects.create(query='Samsung',
                                                     time=timezone.now())
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy A11 (2+32) EU',
            parametrs='(2+32), Черный',
            price=9950,
            url='https://softech.kg/smartfon-samsung-galaxy-a11-232-eu',
            web_site='softech')
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy A21s (3+32) EU',
            parametrs='(3+32), Черный, Синий',
            price=12750,
            url='https://softech.kg/smartfon-samsung-galaxy-a21s-332-eu',
            web_site='softech')
        self.query.products_set.create(
            name='Смартфон Samsung Galaxy A31 (4+128) EU',
            parametrs='(4+128), Черный, Синий',
            price=16950,
            url='https://softech.kg/smartfon-samsung-galaxy-a31-4128-eu',
            web_site='softech')

    @patch(
        'search.services.softech.softech_main.SoftechMain.search_products_for_site')
    def test_error_with_unsaved_query(self, mock):
        """Когда в базе данных не сохранен запрос"""
        mock.return_value = 'Нет товаров, которые соответствуют критериям поиска.'
        self.assertRaises(Exception, SoftechMain().softech_main,
                          'Продукт,которого нет')

    @patch(
        'search.services.softech.softech_main.SoftechMain.search_products_for_site')
    def test_error_with_saved_query(self, mock):
        """Когда в базе данных сохранен запрос"""
        mock.return_value = 'Нет товаров, которые соответствуют критериям поиска.'
        self.assertRaises(Exception, SoftechMain().softech_main, 'Samsung')

    @patch(
        'search.services.softech.softech_main.SoftechMain.search_products_for_site')
    def test_error_with_outdated_request(self, mock):
        """Когда в базе данных сохранен запрос,но он не актуален ,
                т.к он был сохранен более одного дня назад"""
        mock.return_value = 'Нет товаров, которые соответствуют критериям поиска.'
        times = timezone.now() - datetime.timedelta(days=1, seconds=1)
        self.query.time = times
        self.query.save()
        self.assertRaises(Exception, SoftechMain().softech_main,
                          'Samsung Galaxy')

    @patch(
        'search.services.softech.softech_main.SoftechMain.search_products_for_site')
    def test_request_without_products(self, mock):
        """Cлучай,в котором запрос существует, но продукты еще не были созданы к нему"""
        mock.return_value = [
            {
                'name': 'Смартфон Samsung Galaxy A31 (4+128) EU',
                'price': 16950,
                'params': '(4+128), Черный, Синий',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-a31-4128-eu'},
            {
                'name': 'Смартфон Samsung Galaxy M11 (3+32) EU',
                'price': 10950,
                'params': '(3+32), Синий, Фиолетовый',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-m11-332-eu'},
            {
                'name': 'Смартфон Samsung Galaxy A71 (8+128) EU',
                'price': 26950,
                'params': '(8+128), Черный, Синий',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-a71-4128-eu'}
        ]
        products = SoftechMain().softech_main('Samsung')
        self.assertEqual(3, products.count())

    @patch(
        'search.services.softech.softech_main.SoftechMain.search_products_for_site')
    def test_of_creatin_a_request_and_products_to_it(self, mock):
        """Случай,при котором создаетя новый запрос и продукты к нему"""
        mock.return_value = [
            {
                'name': 'Смартфон Samsung Galaxy A11 (2+32) EU',
                'price': 9950,
                'params': '(2+32), Черный',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-a11-232-eu'},
            {
                'name': 'Смартфон Samsung Galaxy A21s (3+32) EU',
                'price': 12750,
                'params': '(3+32), Черный, Синий',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-a21s-332-eu'},
            {
                'name': 'Смартфон Samsung Galaxy A31 (4+128) EU',
                'price': 16950,
                'params': '(4+128), Черный, Синий',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-a31-4128-eu'}
        ]
        products = SoftechMain().softech_main('Samsung Galaxy A')
        self.assertEqual(3, products.count())

    def test_with_pre_created_product_and_request(self):
        """Случай , при котором продукты и запрос уже были созданы"""
        products = SoftechMain().softech_main('Samsung Galaxy')
        self.assertEqual(3, products.count())

    @patch(
        'search.services.softech.softech_main.SoftechMain.search_products_for_site')
    def test_for_outdated_request(self, mock):
        """Случай , при котором продукты и запрос уже были созданы,
            но запрос не актуален ,т.к он был сохранен более одного дня назад"""
        mock.return_value = [
            {
                'name': 'Смартфон Samsung Galaxy A11 (2+32) EU',
                'price': 9950,
                'params': '(2+32), Черный',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-a11-232-eu'},
            {
                'name': 'Смартфон Samsung Galaxy A21s (3+32) EU',
                'price': 12750,
                'params': '(3+32), Черный, Синий',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-a21s-332-eu'},
            {
                'name': 'Смартфон Samsung Galaxy A31 (4+128) EU',
                'price': 16950,
                'params': '(4+128), Черный, Синий',
                'url': 'https://softech.kg/smartfon-samsung-galaxy-a31-4128-eu'}
        ]
        times = timezone.now() - datetime.timedelta(days=1, seconds=1)
        self.query.time = times
        self.query.save()
        products = SoftechMain().softech_main('Samsung Galaxy')
        self.assertEqual(3, products.count())

import unittest
from ...services.myphone.extract import MyphonekgExtractor


class TestLinkCase(unittest.TestCase):
    """Test product link"""

    def test_product_link(self):
        extract = MyphonekgExtractor()
        name_list = ['xiaomi', 'redmi', 'note']
        expectation_link = 'https://myphone.kg/ru/catalog/search?s=xiaomi+redmi+note'
        product_link = extract.get_product_link(name_list=name_list)
        self.assertEqual(expectation_link, product_link)



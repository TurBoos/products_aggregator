# Local html files should be saved via requests library

import unittest
from search.services.myphone.transform import MyphonekgTransformer


class TestProductDetailsCase(unittest.TestCase):
    """Test product details"""

    def test_product_details(self):
        """General case with parameters and colors"""

        transform = MyphonekgTransformer()

        with open('search/tests/myphone/htmls/get_products_file.html',
                  'r') as html_file:
            get_product_page = html_file.read()
        product_details = transform.get_product_details(html=get_product_page)
        expectation_product_details = [
            {'name': 'Xiaomi Redmi 8 64Gb',
             'params': 'Синий Чёрный 64Gb',
             'price': 9990,
             'url': 'https://myphone.kg/ru/catalog/cell/i1881/redmi_8'},

            {'name': 'Xiaomi Redmi 8 32Gb',
             'params': 'Чёрный Синий Красный 32Gb',
             'price': 9190,
             'url': 'https://myphone.kg/ru/catalog/cell/i1853/redmi_8'},

            {'name': 'Xiaomi Redmi 8A Pro 32Gb',
             'params': 'Белый Синий Пурпурный 32Gb',
             'price': 7990,
             'url': 'https://myphone.kg/ru/catalog/cell/i1993/redmi_8a_pro'},
            {'name': 'Xiaomi Redmi 8A 32Gb',
             'params': 'Чёрный Красный Синий 32Gb',
             'price': 7790,
             'url': 'https://myphone.kg/ru/catalog/cell/i1852/redmi_8a'}
        ]

        self.assertEqual(product_details, expectation_product_details)

    def test_product_with_colors(self):
        """Case without parameters, but with colors"""

        transform = MyphonekgTransformer()
        with open(
                'search/tests/myphone/htmls/get_products_with_colors_file.html',
                'r') as html_file:
            get_product_page = html_file.read()
            product_details = transform.get_product_details(
                html=get_product_page)
            expectation_product_details = [{'name': 'Senseit L208',
                                            'params': 'Чёрный',
                                            'price': 2500,
                                            'url': 'https://myphone.kg/ru/catalog/cell/i1293/l208'
                                            },
                                           {'name': 'Senseit L222',
                                            'params': 'Чёрный',
                                            'price': 1950,
                                            'url': 'https://myphone.kg/ru/catalog/cell/i1497/l222'
                                            },
                                           {'name': 'Senseit F1',
                                            'params': 'Чёрный',
                                            'price': 1700,
                                            'url': 'https://myphone.kg/ru/catalog/cell/i1721/f1'
                                            },
                                           {'name': 'Senseit L101',
                                            'params': 'Чёрный',
                                            'price': 1300,
                                            'url': 'https://myphone.kg/ru/catalog/cell/i1780/l101'}]

            self.assertEqual(product_details, expectation_product_details)

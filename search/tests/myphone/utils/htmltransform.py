import requests


def get_products_file():
    response = requests.get('https://myphone.kg/ru/catalog/search?s=Xiaomi+Redmi+8', verify=False)
    with open('search/tests/myphone/htmls/get_products_file.html', 'w') as results_page_html:
        results_page_html.write(response.text)


def get_products_without_params_file():
    response = requests.get('https://myphone.kg/ru/catalog/search?s=Xiaomi+Ninebot+Mini', verify=False)
    with open('get_products_without_params_file.html', 'w') as results_page_html:
        results_page_html.write(response.text)


def get_products_with_colors_file():
    response = requests.get('https://myphone.kg/ru/catalog/search?s=Senseit', verify=False)
    with open('search/tests/myphone/htmls/get_products_with_colors_file.html', 'w') as results_page_html:
        results_page_html.write(response.text)


def get_products_with_params_file():
    response = requests.get('https://myphone.kg/ru/catalog/search?s=Toshiba', verify=False)
    with open('get_products_with_params_file.html', 'w') as results_page_html:
        results_page_html.write(response.text)


def get_products_with_parameters_file():
    response = requests.get('https://myphone.kg/ru/catalog/search?s=Noname', verify=False)
    with open('get_products_with_parameters_file.html', 'w') as results_page_html:
        results_page_html.write(response.text)


def get_products_with_two_price_file():
    response = requests.get('https://myphone.kg/ru/catalog/search?s=Toshiba+Canvio+Basics', verify=False)
    with open('get_products_with_two_price_file.html', 'w') as results_page_html:
        results_page_html.write(response.text)


def get_products_with_full_params_file():
    response = requests.get('https://myphone.kg/ru/catalog/search?s=Hoco+MicroSDHC', verify=False)
    with open('get_products_with_full_params_file.html', 'w') as results_page_html:
        results_page_html.write(response.text)


get_products_file()  # PARAMS FROM DIV CLASS='TITLE'
#get_products_without_params_file()
get_products_with_colors_file()
# get_products_with_params_file()  # PARAMS FROM DIV CLASS='PARAMS'
# get_products_with_parameters_file()  # PARAMS FROM DIV CLASS='TITLE'
# get_products_with_two_price_file()
# get_products_with_full_params_file()  # PARAMS FROM DIV CLASS='TITLE' AND DIV CLASS='PARAMS'

from django import forms


class QueryForm(forms.Form):
    """Search form"""
    query = forms.CharField(max_length=100)

from django.db import models
from django.utils import timezone
import datetime


class Query(models.Model):
    """User Query Table"""
    query = models.CharField(max_length=250, unique=True)
    time = models.DateTimeField()

    def __str__(self):
        return self.query

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.time <= now

class Products(models.Model):
    """Table of data received from the softech.kg"""
    name = models.CharField(max_length=250)
    parametrs = models.TextField()
    price = models.IntegerField(default=0)
    url = models.TextField()
    query_id = models.ForeignKey(Query, on_delete=models.CASCADE)
    web_site=models.CharField(max_length=200)

    def __str__(self):
        return self.name

# Generated by Django 3.0.6 on 2020-05-28 15:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Query',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('query', models.CharField(max_length=250, unique=True)),
                ('time', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='products',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('parametrs', models.TextField()),
                ('price', models.IntegerField(default=0)),
                ('url', models.TextField()),
                ('query_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='search.Query')),
            ],
        ),
    ]

import requests


class Extract(object):

    def do_request(self, url: str) -> str:
        headers = {
            'accept': '*/*',
            'User-Agent':
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) '
                'AppleWebKit/537.36 (KHTML, like Gecko) '
                'Chrome/80.0.3987.149 Safari/537.36'}
        response = requests.get(url=url, headers=headers)
        return response.text

    def get_product_pages(self, product_links: list) -> list:
        product_pages = []
        for url in product_links:
            page = self.do_request(url=url)
            product_pages.append(page)
        return product_pages

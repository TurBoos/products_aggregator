from bs4 import BeautifulSoup


class KivanoTransform(object):

    def form_base_url(self, name_list: list) -> str:
        name = ''
        for arg in name_list:
            name += arg
            if arg != name_list[-1]:
                name += '%20'
        url = 'https://www.kivano.kg/mobilnye-telefony'
        url += '?search=' + name
        return url

    def _create_product_urls(self, number_of_urls: int, url: str) -> list:
        product_links = []
        for i in range(1, number_of_urls + 1):
            product_url = url + f'&page={i}'
            product_links.append(product_url)
        return product_links

    def _get_number_of_urls(self, page: str) -> int:
        soup = BeautifulSoup(page, features='lxml')
        pages = soup.find('li', class_='last')
        if pages == None:
            number_of_urls = 1
        else:
            number_of_urls = int(pages.find('a').text)
        return number_of_urls

    def _get_product_boxes(self, product_pages: list) -> list:
        list_of_product_boxes = []
        for page in product_pages:
            soup = BeautifulSoup(page, features='lxml')
            product_boxes = soup.find_all('div', attrs={'class': 'item product_listbox oh'})
            if product_boxes == []:
                return 'Неправильное название товара'
            for box in product_boxes:
                list_of_product_boxes.append(box)
        return list_of_product_boxes

    def _find_data_from_product_boxes(self, list_of_product_boxes: list) -> list:
        data_list = []
        for box in list_of_product_boxes:
            try:
                strongs = box.find_all('strong')
                if strongs[1].text.split()[0] != 'Скоро':
                    a = strongs[0].find('a')
                    name = a.text
                    link = 'https://www.kivano.kg' + a['href']
                    price = strongs[1].text.split()[0]
                    data_list.append({'name': name,
                                      'price': price,
                                      'url': link})
            except:
                pass
        return data_list

    def get_product_urls_from_requested_page(self, page: str, url: str) -> list:
        number_of_urls = self._get_number_of_urls(page=page)
        product_links = self._create_product_urls(number_of_urls=number_of_urls, url=url)
        return product_links

    def get_data_from_product_pages(self, product_pages: list) -> list:
        product_boxes = self._get_product_boxes(product_pages=product_pages)
        data_list = self._find_data_from_product_boxes(list_of_product_boxes=product_boxes)
        data_list.sort(key=lambda x: int(x['price'].split()[0]))
        return data_list

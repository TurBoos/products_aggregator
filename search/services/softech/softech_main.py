from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from search.services.load import DatabaseLoad
from search.services.softech.softech_transform import SoftechTransform
from search.services.softech.softech_exctract import SoftechExtractor
from search.models import Query, Products
import logging

logger = logging.getLogger('django')


class SoftechMain:
    def search_products_for_site(self, query):
        Exctractor = SoftechExtractor()
        product_link = Exctractor.get_page(name_list=query)
        if 'Oops' in product_link:
            return product_link
        if product_link == 'Нет товаров, которые соответствуют критериям поиска.':
            return 'Нет товаров, которые соответствуют критериям поиска.'
        Transform = SoftechTransform()
        product_catalog = Transform.response_to_user(url=product_link)
        return product_catalog

    def check_for_relevance_and_withdrawal_of_products(self, user_query_name):
        logger.info(f'Продукты по запросу {user_query_name} были найдены ')
        logger.info(f'Проверка запроса {user_query_name} на актуальность')
        was_published_recently = user_query_name.was_published_recently()
        if was_published_recently == True:
            logger.info(
                f'Запрос {user_query_name} актуален,изменения не требуются')
            logger.info(
                f'Вывод продуктов из базы данных по запросу {user_query_name}')
            return Products.objects.filter(query_id=user_query_name,
                                           web_site='softech')
        logger.info(f'Запрос {user_query_name} не актуален')
        logger.info(f'Обновление времени {user_query_name} в базе данных')
        user_query_name.time = timezone.now()
        user_query_name.save()
        logger.info(
            f'Получение данных с сайта softech по запросу {user_query_name}')
        site_response = self.search_products_for_site(
            query=str(user_query_name))
        if type(site_response) != str:
            logger.info(
                f'Удаление старых продуктов по запросу {user_query_name}')
            user_query_name.products_set.all().delete()
            logger.info(
                f'Загрузка новых продуктов в бд по запросу {user_query_name}')
            DatabaseLoad().load_products_in_models(
                products=site_response,
                query=user_query_name,
                web_site='softech')
            logger.info(f'Вывод продуктов по запросу {user_query_name}')
            return Products.objects.filter(query_id=user_query_name,
                                           web_site='softech')
        else:
            logger.info(f'Товар не был найден по запросу {user_query_name}')
            raise Exception(site_response)

    def creating_products_for_the_request_and_displaying_them(self,
                                                              user_query_name):
        logger.info(f'Продукты по запросу {user_query_name} нет в базе данных')
        logger.info(
            f'Получение данных с сайта softech по запросу {user_query_name}')
        site_response = self.search_products_for_site(
            query=str(user_query_name))
        if type(site_response) != str:
            logger.info(
                f'Загрузка новых продуктов в бд по запросу {user_query_name}')
            DatabaseLoad().load_products_in_models(
                products=site_response,
                query=user_query_name,
                web_site='softech')
            logger.info(f'Вывод продуктов по запросу {user_query_name}')
            return Products.objects.filter(query_id=user_query_name,
                                           web_site='softech')
        else:
            logger.info(f'Товар не был найден по запросу {user_query_name}')
            raise Exception(site_response)

    def creating_a_request_and_products_for_it(self, query):
        logger.info(f'Запрос {query} не зарегистрирован в базе данных')
        logger.info(f'Получение данных с сайта softech по запросу {query}')
        site_response = self.search_products_for_site(query=str(query))
        if type(site_response) != str:
            logger.info(f'Создание нового запроса {query} в базе данных')
            new_query = Query.objects.create(query=query, time=timezone.now())
            logger.info(f'Загрузка новых продуктов в бд по запросу {query}')
            DatabaseLoad().load_products_in_models(
                products=site_response,
                query=new_query,
                web_site='softech')
            logger.info(f'Вывод продуктов по запросу {query}')
            return Products.objects.filter(query_id=new_query,
                                           web_site='softech')
        else:
            logger.info(f'Товар не был найден по запросу {query}')
            raise Exception(site_response)

    def softech_main(self, query):
        logger.info('-----------------------------\n'
                    'Выполняется скрипт softech.kg')
        try:
            logger.info(f'Получение {query} из базы данных')
            user_query_name = Query.objects.get(query=query)
            logger.info(f'Поиск продуктов по запросу {query} в базе данных')
            if Products.objects.filter(query_id=user_query_name,
                                       web_site='softech'):
                result = self.check_for_relevance_and_withdrawal_of_products \
                    (user_query_name=user_query_name)
                return result
            else:
                result = self.creating_products_for_the_request_and_displaying_them \
                    (user_query_name=user_query_name)
                return result
        except ObjectDoesNotExist:
            result = self.creating_a_request_and_products_for_it \
                (query=query)
            return result

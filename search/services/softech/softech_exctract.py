from bs4 import BeautifulSoup
import requests

url = 'https://softech.kg/index.php?route=product/search&search='


class SoftechExtractor:
    def get_url(self, product_name):  # получение,трансформация и вывод готовой url
        response = \
            (url + '%20'.join(str(product_name).split())
             + '&category_id=59&sub_category=true&limit=25')
        return response

    def product_availability(self, product_html):  # поиск наличия на странице товара
        soup = BeautifulSoup(product_html, 'lxml')
        if 'Уточняйте цену и наличие' in str(soup.find('span', class_='out-stock')):  # Если товара нет
            return 'Уточняйте цену и наличие'
        elif "Есть в наличии" in str(soup.find('span', class_='stock')):
            return 'Есть в наличии'

    def get_page(self, name_list: list) -> str:  # поиск url продуктов
        products_url = []
        final_product_url = []
        try:
            response = requests.get\
                (self.get_url(product_name=name_list), timeout=5)
            response.raise_for_status()
            soup = BeautifulSoup\
                (requests.get(self.get_url(product_name=name_list)).text, 'lxml')
            for product_url in soup.find_all('div', class_='name'):
                product_url = product_url.find('a').attrs['href']
                products_url.append(product_url)
            for url in products_url:
                availability = self.product_availability\
                    (product_html=requests.get(url).text)
                if 'Уточняйте цену и наличие' not in availability:
                    final_product_url.append(requests.get(url).text)
                else:
                    break
            if final_product_url != []:
                return final_product_url
            else:
                return 'Нет товаров, которые соответствуют критериям поиска.'
        except requests.exceptions.ConnectionError:
            return 'Упс. Похоже, поиск DNS не удался..'

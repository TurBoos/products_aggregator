from bs4 import BeautifulSoup


class SoftechTransform:

    def url(self, product_page):
        url = []
        soup = BeautifulSoup(product_page, 'lxml')
        for href2 in soup.find('ul', class_="breadcrumb"):
            url.append(href2.find('a'))
        return url[-2].attrs['href']

    def find_url_names(self, url):
        products_url = []
        for product_url in url:  # перехожу по ссылкам на товары
            products_url.append(self.url(product_page=product_url))
        if products_url != []:
            return products_url
        return ['Нет ссылки']

    def name(self, product_page):
        names = []
        soup = BeautifulSoup(product_page, 'lxml')
        for name in soup.find_all('h1', class_="product-title"):
            names.append(str(name.string).strip())
        return names

    def find_name(self, url):
        product_names = []
        for product_url in url:  # перехожу по ссылкам на товары
            product_names.append(self.name(product_page=product_url))
        if product_names != []:
            return product_names  # вывожу список цветов
        return ['Нет имени']

    def colors_and_memory(self, product_page):  # поиск цвета на странице товара
        colors_and_memory = []
        soup = BeautifulSoup(product_page, 'lxml')
        find_memory = soup.find('h1', class_='product-title')
        find_memory = str(find_memory.string).strip()
        for memory in find_memory.split():
            if '+' in memory:
                if len(memory) > 1:  # для телефонов,телевизоров и т.д
                    colors_and_memory.append(memory)
            elif 'gb' in memory.lower():  # для флеш карт
                colors_and_memory.append(memory)
            if 'tb' in memory.lower():
                colors_and_memory.append(memory)
        for color_page in soup.find_all('div', class_='radio'):
            colors_and_memory.append(str(color_page.text).strip())
        if len(colors_and_memory) == 0:
            colors_and_memory.append('none')
        return colors_and_memory

    def find_color_and_memory(self, url):
        product_colors = []
        for product_url in url:  # перехожу по ссылкам на товары
            product_colors.append(self.colors_and_memory(product_page=product_url))
        if product_colors != []:
            return product_colors  # вывожу список цветов
        return ['Нет характеристик']

    def prices(self, product_page):
        prices = []
        soup = BeautifulSoup(product_page, 'lxml')
        for price in soup.find_all('div', class_='price-section'):
            price = str(price.text).split()
            prices.append(str((price[0])))
        return prices

    def find_price(self, url):  # поиск цены
        product_prices = []
        for product_url in url:  # перехожу по ссылкам на товары
            product_prices.append(self.prices(product_page=product_url))
        if product_prices != []:
            return product_prices
        return ['Нет цены']

    def response_to_user(self, url):  # конечный ответ
        general_characteristics = []
        zipped = zip(self.find_name(url=url),
                     self.find_price(url=url),
                     self.find_color_and_memory(url=url),
                     self.find_url_names(url=url))
        for elem in zipped:
            characteristics = {
                'name': ' '.join(elem[0]),
                'price': int(elem[1][0]),
                'params': ', '.join(elem[2]),
                'url': str(elem[3])
            }
            general_characteristics.append(characteristics)
        return general_characteristics

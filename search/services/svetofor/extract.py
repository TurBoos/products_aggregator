import requests


class Extract(object):

    def do_request(self, url):
        headers = {'accept': '*/*',
                   'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'}
        response = requests.get(url=url, headers=headers)
        return response.text

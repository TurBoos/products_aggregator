from bs4 import BeautifulSoup


class SvetoforTransform(object):

    def form_base_url(self, name_list: list) -> str:
        name = ''
        for arg in name_list:
            name += arg
            if arg != name_list[-1]:
                name += '+'
        url = 'https://svetofor.info/sotovye-telefony-i-aksessuary/vse-smartfony'
        url += '?q=' + name + '&items_per_page=100'
        return url

    def parse_page(self, product_page: str) -> list:
        data_list =[]
        soup = BeautifulSoup(product_page, features='lxml')
        product_boxes = soup.find_all('div', class_='ty-column4')
        if product_boxes == []:
            return 'Неправильное имя'
        for box in product_boxes:
            try:
                titles = box.find_all('a')
                title = titles[0].find('img')['title']    # title = titles[0].text.strip()
                link = titles[1]['href']
                prices = box.find_all('span', class_='ty-price-num')
                price = prices[0].text
                data_list.append({'name': title,
                                  'price': price,
                                  'url': link})
            except:
                pass
        data_list.sort(key=lambda x: int(x['price'].split()[0]))
        return data_list

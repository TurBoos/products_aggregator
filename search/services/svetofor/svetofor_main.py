import logging
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from search.models import Query, Products
from search.services.load import DatabaseLoad
from .extract import Extract
from .transform import SvetoforTransform

logger = logging.getLogger('django')


class SvetovorMain:
    def search_products_for_site(self, user_query_name):
        name_list = str(user_query_name).split()
        extract = Extract()
        transform = SvetoforTransform()
        base_url = transform.form_base_url(name_list=name_list)
        requested_page = extract.do_request(url=base_url)
        products_data = transform.parse_page(product_page=requested_page)
        return products_data

    def check_for_relevance_and_withdrawal_of_products(self, query):
        logger.info(f'Продукты по запросу {query} были найдены ')
        logger.info(
            f'Проверка запроса {query} на актуальность времени')
        was_published_recently = query.was_published_recently()
        if was_published_recently == True:
            logger.info(
                f'Запрос {query} актуален,изменения не требуются')
            logger.info(
                f'Вывод продуктов из базы данных по запросу {query}')
            return Products.objects.filter(query_id=query,
                                           web_site='svetofor')
        else:
            logger.info(f'Запрос {query} не актуален')
            logger.info(
                f'Обновление времени {query} в базе данных')
            query.time = timezone.now()
            query.save()
            logger.info(
                f'Получение данных с сайта svetofor по запросу {query}')
            site_result = self.search_products_for_site(
                user_query_name=query)
            if site_result == 'Неправильное имя':
                logger.info(
                    f'Товар не был найден по запросу {query}')
                raise Exception('Неправильное имя')
            logger.info(
                f'Удаление старых продуктов по запросу {query}')
            query.products_set.all().delete()
            load = DatabaseLoad()
            logger.info(
                f'Загрузка новых продуктов в бд по запросу {query}')
            load.load_products_in_models(products=site_result,
                                         query=query,
                                         web_site='svetofor')
            logger.info(f'Вывод продуктов по запросу {query}')
            return Products.objects.filter(query_id=query,
                                           web_site='svetofor')

    def creating_products_for_the_request_and_displaying_them(self, query):
        logger.info(
            f'Продукты по запросу {query} нет в базе данных')
        logger.info(
            f'Получение данных с сайта svetofor по запросу {query}')
        site_result = self.search_products_for_site(
            user_query_name=query)
        if site_result == 'Неправильное имя':
            logger.info(
                f'Товар не был найден по запросу {query}')
            raise Exception('Неправильное имя')
        logger.info(
            f'Загрузка новых продуктов в бд по запросу {query}')
        DatabaseLoad().load_products_in_models(
            products=site_result,
            query=query,
            web_site='svetofor')
        logger.info(f'Вывод продуктов по запросу {query}')
        return Products.objects.filter(query_id=query,
                                       web_site='svetofor')

    def creating_a_request_and_products_for_it(self, user_query_name):
        logger.info(
            f'Запрос {user_query_name} не зарегистрирован в базе данных')
        logger.info(
            f'Получение данных с сайта svetofor по запросу {user_query_name}')
        site_result = self.search_products_for_site(
            user_query_name=user_query_name)
        if site_result == 'Неправильное имя':
            logger.info(f'Товар не был найден по запросу {user_query_name}')
            raise Exception('Неправильное имя')
        logger.info(
            f'Создание нового запроса {user_query_name} в базе данных')
        new_query = Query.objects.create(query=user_query_name,
                                         time=timezone.now())
        logger.info(
            f'Загрузка новых продуктов в бд по запросу {user_query_name}')
        DatabaseLoad().load_products_in_models(
            products=site_result,
            query=new_query,
            web_site='svetofor')
        logger.info(f'Вывод продуктов по запросу {user_query_name}')
        return Products.objects.filter(query_id=new_query,
                                       web_site='svetofor')

    def svetofor_main(self, user_query_name):
        logger.info('--------------------------------\n'
                    'Выполняется скрипт svetofor.info')
        try:
            logger.info(f'Получение {user_query_name} из базы данных')
            query = Query.objects.get(query=user_query_name)
            logger.info(f'Поиск продуктов по запросу {query} в базе данных')
            if Products.objects.filter(query_id=query, web_site='svetofor'):
                result = self.check_for_relevance_and_withdrawal_of_products \
                    (query=query)
                return result
            else:
                result = self.creating_products_for_the_request_and_displaying_them \
                    (query=query)
                return result
        except ObjectDoesNotExist:
            result = self.creating_a_request_and_products_for_it \
                (user_query_name=user_query_name)
            return result

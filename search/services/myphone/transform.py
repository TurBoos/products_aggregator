from bs4 import BeautifulSoup as bs

HOST = 'https://myphone.kg'


class MyphonekgTransformer:
    def _get_product_name(self, item):
        product = item.find('div', attrs={'class': 'title'})
        product_name = product.find('a')
        return product_name

    def _get_product_params(self, item):
        product = item.find('div', attrs={'class': 'title'})
        param = product.find('span')
        if param is not None:
            param = param.text
            return param

    def _get_product_colors(self, item):
        params = item.find('div', attrs={'class': 'params'})
        if params is not None:
            colors = params.find('div', attrs={'class': 'color'})
            return colors

    def _get_product_price(self, item):
        price = item.find('div', attrs={'class': 'price'})
        if price is not None:
            price = price.find_next('span')
            price = price.get_text()
            price = ''.join(price.split())
            price = int(price)
            return price

    def get_product_details(self, html):
        soup = bs(html, 'html.parser')
        items = soup.find_all('div', attrs={'class': 'block-content'})
        product_details = []

        for item in items:
            param_list = []

            product = self._get_product_name(item)
            name = ''
            url = ''

            if product is not None:
                name = product.text
                name = ' '.join(name.split())

                url = HOST + product.attrs['href']

            param = self._get_product_params(item)

            colors = self._get_product_colors(item)
            if colors is not None:
                for color in colors.find_all('span'):
                    color = color.attrs['title']
                    param_list.append(color)

            if param is not None:
                param_list.append(param)
            params = ' '.join(param_list)

            price = self._get_product_price(item)

            if (price is not None) and (url.find('cell') != -1):
                product_details.append({'name': name,
                                        'params': params,
                                        'price': price,
                                        'url': url
                                        })
        if not product_details:
            return 'Товар не найден'

        else:
            return product_details








































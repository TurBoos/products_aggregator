import requests
import datetime

URL = 'https://myphone.kg/ru/catalog/search?s='
HEADERS = {'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0',
           'accept': '*/*'}


class MyphonekgExtractor:

    def get_product_link(self, name_list: list) -> str:
        product_name = ''
        for arg in name_list:
            product_name += arg
            if arg != name_list[-1]:
                product_name += '+'
        product_link = URL + product_name
        return product_link

    def get_html(self, url: str) -> str:
        try:
            response = requests.get(url=url, headers=HEADERS)
            response.raise_for_status()
            return response.text

        except requests.ConnectionError:
            print("Ошибка соединения")
            exit()


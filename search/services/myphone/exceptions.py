# from django.core import exceptions
# ls=list(dir(exceptions))
# for i in ls:
#     print(i)


class ProductNotFound(Exception):
    def __init__(self, msg):
        super().__init__(msg)
import logging
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from search.models import Query, Products
from .transform import MyphonekgTransformer
from .extract import MyphonekgExtractor
from .exceptions import ProductNotFound
from ..load import DatabaseLoad

logger = logging.getLogger('django')


class MyphoneMain:
    def get_data_from_site(self, user_query_name):
        query_name = str(user_query_name).split()
        extractor = MyphonekgExtractor()
        link = extractor.get_product_link(name_list=query_name)
        page = extractor.get_html(url=link)
        transform = MyphonekgTransformer()
        products = transform.get_product_details(html=page)
        return products

    def check_for_relevance_and_withdrawal_of_products(self, query):
        logger.info(f'Продукты по запросу {query} были найдены ')
        logger.info(
            f'Проверка запроса {query} на актуальность времени')
        was_published_recently = query.was_published_recently()
        if was_published_recently == True:
            logger.info(
                f'Запрос {query} актуален,изменения не требуются')
            logger.info(
                f'Вывод продуктов из базы данных по запросу {query}')
            return Products.objects.filter(query_id=query,
                                           web_site='myphone')
        else:
            logger.info(f'Запрос {query} не актуален')
            logger.info(
                f'Обновление времени {query} в базе данных')
            query.time = timezone.now()
            query.save()
            logger.info(
                f'Получение данных с сайта myphone по запросу {query}')
            result = self.get_data_from_site(query)
            if result == 'Товар не найден':
                logger.info(
                    f'Товар не был найден по запросу {query}')
                raise ProductNotFound('Товар не найден')
            logger.info(
                f'Удаление старых продуктов по запросу {query}')
            query.products_set.all().delete()
            load = DatabaseLoad()
            logger.info(
                f'Загрузка новых продуктов в бд по запросу {query}')
            load.load_products_in_models(products=result, query=query,
                                         web_site='myphone')
            logger.info(f'Вывод продуктов по запросу {query}')
            return Products.objects.filter(query_id=query,
                                           web_site='myphone')

    def creating_products_for_the_request_and_displaying_them(self, query):
        logger.info(
            f'Продукты по запросу {query} нет в базе данных')
        logger.info(
            f'Получение данных с сайта myphone по запросу {query}')
        result = self.get_data_from_site(query)
        if result == 'Товар не найден':
            logger.info(
                f'Товар не был найден по запросу {query}')
            raise ProductNotFound('Товар не найден')
        logger.info(f'Обновление времени по запросу {query}')
        query.time = timezone.now()
        query.save()
        logger.info(
            f'Добавление новых продуктов в бд по запросу {query}')
        load = DatabaseLoad()
        logger.info(
            f'Загрузка новых продуктов в бд по запросу {query}')
        load.load_products_in_models(products=result, query=query,
                                     web_site='myphone')
        logger.info(f'Вывод продуктов по запросу {query}')
        return Products.objects.filter(query_id=query,
                                       web_site='myphone')

    def creating_a_request_and_products_for_it(self, user_query_name):
        logger.info(
            f'Запрос {user_query_name} не зарегистрирован в базе данных')
        logger.info(
            f'Получение данных с сайта myphone по запросу {user_query_name}')
        result = self.get_data_from_site(user_query_name)
        if result == 'Товар не найден':
            logger.info(f'Товар не был найден по запросу {user_query_name}')
            raise ProductNotFound('Товар не найден')
        logger.info(
            f'Создание нового запроса {user_query_name} в базе данных')
        new_query = Query.objects.create(query=user_query_name,
                                         time=timezone.now())
        logger.info(
            f'Загрузка новых продуктов в бд по запросу {user_query_name}')
        load = DatabaseLoad()
        load.load_products_in_models(products=result, query=new_query,
                                     web_site='myphone')
        logger.info(f'Вывод продуктов по запросу {user_query_name}')
        return Products.objects.filter(query_id=new_query,
                                       web_site='myphone')

    def myphonekg_main(self, user_query_name):
        logger.info('------------------------------\n'
                    'Выполняется скрипт myphone.kg')
        try:
            logger.info(f'Получение {user_query_name} из базы данных')
            query = Query.objects.get(query=user_query_name)
            logger.info(f'Поиск продуктов по запросу {query} в базе данных')
            if Products.objects.filter(query_id=query, web_site='myphone'):
                result = self.check_for_relevance_and_withdrawal_of_products \
                    (query=query)
                return result
            else:
                result = self.creating_products_for_the_request_and_displaying_them \
                    (query=query)
                return result
        except ObjectDoesNotExist:
            result = self.creating_a_request_and_products_for_it \
                (user_query_name=user_query_name)
            return result

# ***Smart search***

## Начальная настройка:

```
git clone
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Запуск сервера ***(локально)***

- Cоздание базы данных:
  - `sudo docker-compose up -d db`
- Настройка *[.env](.env)* файла:
  - `DATABASE_HOST = 127.0.0.1`
- Запуск проэкта:
  - `python manage.py runserver`
  
### Запуск тестов

`python manage.py test search.tests`

### Запуск *docker-compose.yml*

- Настройка *[.env](.env)* файла:
  - `DATABASE_HOST= db`
- Запуск *doker-compose*
  - `sudo docker-compose up --build`





